document.addEventListener("DOMContentLoaded", function () {
  const list = [];

  const form = document.querySelector(".tourist-container-form");
  const imageInput = document.querySelector(".label-image");
  const textsInput = document.querySelector(".tourist-texts");
  const places = document.querySelector(".places-List");

  form.addEventListener("submit", addPlaceToList);

  function addPlaceToList(event) {
    event.preventDefault();

    const image = event.target["image"].value;
    const title = event.target["input-title"].value;
    const paragraph = event.target["input-paragraph"].value;

    if (title != "") {
      const place = {
        placeImage: image,
        placeTitle: title,
        placeDescription: paragraph,
      };
      list.push(place);
      renderPlaceItems();
      //   resetInputs();

      console.log(list);
      console.log(image);
    }
  }

  function renderPlaceItems() {
    let placesStructure = "";

    list.forEach(function (place) {
      placesStructure += `
          <li class="places-Cards>
            <span>${place.placeImage}</span>
            <span>${place.placeTitle}</span>
            <span>${place.placeDescription}</span>
          </li>
          `;
    });

    places.innerHTML = placesStructure;

    // function resetInputs {
    //     imageInput = '';
    //     textsInput = "";
    // }
  }
});
